import numpy as np 

class Agent:
	def __init__(self, *args, **kwargs):
		raise NotImplementedError()

	def act(self, *args, **kwargs):
		raise NotImplementedError()

	def train(self, *args, **kwargs):
		raise NotImplementedError()

class QLearning(Agent):
	def __init__(self, observation_space, action_space):
		self.observation_space = observation_space
		self.action_space = action_space

		self.q_table = np.zeros([observation_space.n, action_space.n])

		# Hyperparameters
		self.alpha = 0.1
		self.gamma = 0.6
		self.epsilon = 0.1
		
	def act(self, state):
		greedy_action = np.argmax(self.q_table[state])
		return self.action_space.sample() if np.random.random() < self.epsilon else greedy_action

	def train(self, state, action, reward, next_state, done):
		old_value = self.q_table[state, action]
		next_max = np.max(self.q_table[next_state])
		self.q_table[state, action] += self.alpha * (reward + self.gamma * next_max - old_value)


class SARSA(Agent):
	def __init__(self, observation_space, action_space):
		self.observation_space = observation_space
		self.action_space = action_space

		self.q_table = np.zeros([observation_space.n, action_space.n])

		self.steps = 0
		self.next_action = None

		# Hyperparameters
		self.alpha = 0.1
		self.gamma = 0.6
	
	def act(self, state):
		return self._epsilon_greedy(state) if self.next_action is None else self.next_action
		
	def train(self, state, action, reward, next_state, done):
		old_value = self.q_table[state, action]

		self.next_action = None
		next_value = 0

		if not done:
			self.next_action = self._epsilon_greedy(next_state)
			next_value = self.q_table[next_state, self.next_action]
		
		self.q_table[state, action] += self.alpha * (reward + self.gamma * next_value - old_value)

	def _epsilon_greedy(self, state):
		# Decrease epsilon over time
		epsilon = 1.0 / np.sqrt(self.steps + 1)
		self.steps += 1

		greedy_action = np.argmax(self.q_table[state])
		return self.action_space.sample() if np.random.random() < epsilon else greedy_action


