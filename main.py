import gym
from agent import QLearning, SARSA
from time import sleep

def print_frame(frame):
	print(frame["frame"])
	print(f"Episode: {frame['episode']}")
	print(f"State: {frame['state']}")
	print(f"Action: {frame['action']}")
	print(f"Reward: {frame['reward']}")
	sleep(0.1)

def main():
	"""
	The Task:
	There are 4 locations labeled by R, G, B, Y.
	The task is to pick up passenger at pick-up location (blue) and
	drop off passenger at destination (purple).
	Agent recives +20 for successful drop off.
	Agent losses -1 for every time step.
	Agent losses -10 for illegal pickup and drop off.

	State Space:
	The taxi can be at 25 possible locations.
	The passenger can be at 5 locations: R, G, B, Y, Taxi.
	There are 4 possible possible destinations: R, G, B, Y.
	There are a total of 25 * 5 * 4 = 500 states.

	Action Space:
	There are 6 possible actions: S (0), N (1), E (2), W (3), Pickup (4), Dropoff (5)
	"""

	# Create Environment
	env = gym.make("Taxi-v3").env

	# Create Agent
	agent = QLearning(env.observation_space, env.action_space)

	episodes = 5000
	for episode in range(episodes):

		# Initialize start state
		state = env.reset()

		done = False
		while not done:

			# Perform action
			action = agent.act(state)
			next_state, reward, done, info = env.step(action)

			# Show the last 10 episodes
			if episode > episodes - 10:
				print_frame({
					"frame": env.render(mode="ansi"),
					"episode": episode + 1,
					"state": next_state,
					"action": action,
					"reward": reward
				})

			agent.train(state, action, reward, next_state, done)
			state = next_state


if __name__ == '__main__':
	main()